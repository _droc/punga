# Esto baja los torrents, hay que ponerlo en el crontab y tocar la "config", que usa dailytvtorrents.
# guarda un queue pickleado en queue-show-name.pik, y baja todos los torrents que no figuren en ese queue.
# Eso significa que se pueden borrar los .torrent despues, sin que este coso los baje de nuevo. Eso si,
# si se rompe el .pik, se baja todo de nuevo.
# - acid

import re
import os
import pickle
import urllib
import urllib2

config = { 'prefer': 720,
           'wait': 6,
           'minage': 2,
           'onlynew': 'yes' }

shows = ["the-walking-dead"]

params = urllib.urlencode(config)
torrent_url_re = re.compile('<enclosure url="(.*?)" length="', re.MULTILINE | re.DOTALL)

def safe_write(queue_filename, queue):
    tmp_queue_filename = queue_filename + ".tmp"
    with open(tmp_queue_filename, "w") as fh:
        pickle.dump(queue, fh)
    os.rename(tmp_queue_filename, queue_filename)

for show in shows:
    queue_filename = "queue-" + show + ".pik"
    queue = []
    if os.path.exists(queue_filename):
        fh = open(queue_filename, "r")
        queue = pickle.load(fh)
        fh.close()
    req = urllib2.urlopen('http://www.dailytvtorrents.org/rss/show/' + show + '?' + params)
    rss = req.read()
    torrents = torrent_url_re.findall(rss)
    for torrent in torrents:
        if torrent not in queue:
            queue.append(torrent)
            print "[+] Downloading " + urllib.unquote(torrent)
            req2 = urllib2.urlopen(urllib.unquote(torrent))
            fh = open(urllib.unquote(torrent).split('/')[-1], "wb")
            fh.write(req2.read())
            fh.close()

    safe_write(queue_filename, queue)