# Esto espera un nombre de un video como primer parametro, calcula un hash y lo busca en
# subtitulosenespanol.com (que tiene un metodo para buscar con hash), despues parseando el
# html a lo bruto (es decir, se puede romper en un tiempo cuando cambien el output)., consigue
# el link al subtitulo, lo baja y lo renombra a nombre_video.txt, que en mi caso es lo que necesito
# para que mi tele los lea.
# - acid

import re
import os
import sys, traceback
import struct
import urllib2
import zipfile

def calculateOSDBHash(filename):
    # borrowed from http://subdownloader.googlecode.com/svn-history/r181/trunk/videofile.py
    try:
        longlongformat = 'LL'  # signed long, unsigned long
        bytesize = struct.calcsize(longlongformat)

        filesize = os.path.getsize(filename)
        hash = filesize
        f = file(filename, "rb")
        if filesize < 65536 * 2:
            return "SizeError"

        for x in range(65536/bytesize):
            buffer = f.read(bytesize)
            (l2, l1)= struct.unpack(longlongformat, buffer)
            l_value = (long(l1) << 32) | long(l2)
            hash += l_value
            hash &= 0xFFFFFFFFFFFFFFFF#to remain as 64bit number

        f.seek(max(0,filesize-65536),0)
        for x in range(65536/bytesize):
            buffer = f.read(bytesize)
            (l2, l1) = struct.unpack(longlongformat, buffer)
            l_value = (long(l1) << 32) | long(l2)
            hash += l_value
            hash &= 0xFFFFFFFFFFFFFFFF

        f.close()
        returnedhash =  "%016x" % hash
        return returnedhash

    except IOError:
        return "IOError"

print ""
print "** Simplest subfinder **"
print ""

if not len(sys.argv) == 2:
    print "Usage: ", sys.argv[0], "episode_video_file.ext"
    sys.exit(0)


url_site = "http://www.subtitulosespanol.com/"
url_prefix = "hash-"
url_postfix = "-auto"

print "[+] Calculating hash..."
filehash = calculateOSDBHash(sys.argv[1])

print "[+] Searching subtitulosenespanol.com by hash.."
try:
    req = urllib2.urlopen(url_site + url_prefix + filehash + url_postfix)
    html = req.read()
    subs_info_re = re.compile('<table class="tbResults">.*?"width:540px;" title="">(.*?)</td>.*?<\/table>.*?<div class="divBigBtn"><a.*?href="(.*?)".*?<\/table>', re.MULTILINE | re.DOTALL)
    subs_info = subs_info_re.findall(html)

    print "[+] Getting getsubtitle.com link..."
    sublink_url = url_site + subs_info[0][1]
    req = urllib2.urlopen( sublink_url )
    html = req.read()
    get_zip_re = re.compile('(www.getsubtitle.com.*?)"', re.MULTILINE | re.DOTALL)
    getsubtitle_link = get_zip_re.findall( html )[0]
    getsubtitle_link = "http://" + getsubtitle_link.replace("&amp;", "&")
except:
    print "[!] Problem getting the subtitle.. subtitle not in subtitulosenespanol.com?"
    sys.exit(0)

print "[+] Downloading subtitle..."
req = urllib2.urlopen(getsubtitle_link)
fh = open("subtitle.zip", "wb")
fh.write(req.read())
fh.close()

found = False
print "[+] Searching for srt files within the zip file"
try:
    zfile = zipfile.ZipFile("subtitle.zip")
    for name in zfile.namelist():
        (dirname, filename) = os.path.split(name)
        if ".srt" in filename:
            if not dirname:
                dirname = "."
            print "[+] Decompressing " + filename + " on " + dirname
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            fd = open(name,"w")
            fd.write(zfile.read(name))
            fd.close()
            os.rename(name, os.path.splitext(sys.argv[1])[0] + ".txt")
            found = True
            print "[*] Operation completed successfully!"
    if not found:
        print "[!] .srt file not found in zip file :("
except:
    print "Exception in user code:"
    print '-'*60
    traceback.print_exc(file=sys.stdout)
    print '-'*60
os.unlink("subtitle.zip")